import React, { useState } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import styled from '@emotion/styled';
import { useDispatch, useSelector } from 'react-redux';
import { showAlert, updateChangeEmail } from '../../Redux/Actions/FormActions';


const InputDiv = styled.div`
    width: 15rem;
    margin-top: 1.5rem;
    input{
    border: 1px solid var(--grey1);
    height:2.2rem;
    margin-top: .7rem;
    font-size: 1.1rem;
    color: var(--grey2);
    width: 16rem;
    :hover{
        cursor: pointer;
    }
}
`;

const Labels = styled.label`
    font-weight: bold;
`;


const DivTabs = styled.div`
    width: 35rem;
    @media(max-width: 768px){
        max-width: 100%;
    }
`;

const ButtonDiv = styled.button`
    display: block;
    font-weight: 700;
    text-transform: uppercase;
    border: 1px solid #d1d1d1;
    padding: .8rem 2rem;
    margin: 2rem auto;
    text-align: center;
    background-color: var(--orange);
    &::last-of-type{
        margin-right: 0;
    }

    &:hover{
        cursor: pointer;
    }
`;

const SelectInput = styled.select`
    width: 100%;
    height: 2rem;
    -webkit-appearance: none;
    border: 1px solid var(--grey2);
    border-radius: 5%;
    margin-top: 1rem;
`;

const ErrorMessage = styled.p`
    background-color: red;
    border: 1px solid var(--grey2);
    height: 3rem;
    width: 53vh;
    padding: 0.5rem 2rem 0.5rem 2rem;
    text-align: center;
    font-weight: 700;
    margin: 1rem 50%;
    @media(max-width:768px){
        width: 16rem;
        margin: 3% 1%;
    }
`;





const TabsTable = () => {

    // State of the component
    const [email, changeEmail] = useState('');
    const [password, changePassword] = useState('');
    const [confirmpassword, saveConfirmPassword] = useState('');
    const [changefirstname, saveChangeFirstName] = useState('');
    const [changelastname, saveChangeLastName] = useState('');
    const [modifyaddress, saveModifyAddress] = useState('');
    const [country, selectCountry] = useState('');
    const [error, saveError] = useState(false);


    const dispatch = useDispatch();

    // Accesing store´s state
    const errorMostrar = useSelector(state => state.error);
    // console.log(errorMostrar);
    // Calling action 
    // const dispatchUpdate = UserInformation => dispatch(updateChangeEmail(UserInformation));

    const dispatchUpdate = UserInformation => dispatch(showAlert(UserInformation));


    // If the user submit the update info button
    const UpdateInformation = e => {
        e.preventDefault();

        try {
            if (password.trim() !== confirmpassword.trim()) {
                saveError(true);
            } else if (email !== (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i)) {
                // saveError(true);
            }
        } catch (error) {
            error(false);

        }




        dispatchUpdate({
            email,
            password,
            confirmpassword,
            changefirstname,
            changelastname,
            modifyaddress,
            country,
            error
        });



    }



    return (
        <>
            <form
                onSubmit={UpdateInformation}
            >
                <DivTabs>
                    <Tabs>
                        <TabList>
                            <Tab>Main Information</Tab>
                            <Tab>Additional Information</Tab>
                        </TabList>
                        {/* Main Information */}
                        <TabPanel>
                            {/* Change e-mail address */}
                            <InputDiv>
                                <Labels htmlFor="input">Change Your Email</Labels>
                                <input
                                    type="text"
                                    name="change your email"
                                    placeholder="Change Your Email Address"
                                    value={email}
                                    onChange={e => changeEmail(e.target.value)}
                                />
                            </InputDiv>

                            {/* Change password */}
                            <InputDiv>
                                <Labels htmlFor="input">Change Your Password</Labels>
                                <input
                                    type="password"
                                    name="change your password"
                                    placeholder="Change Your Password"
                                    value={password}
                                    onChange={e => changePassword(e.target.value)}
                                />
                            </InputDiv>

                            {/* Confirm password */}
                            <InputDiv>
                                <Labels htmlFor="input">Confirm Your Password</Labels>
                                <input
                                    type="password"
                                    name="confirmpassword"
                                    placeholder="Confirm Your Password"
                                    value={confirmpassword}
                                    onChange={e => saveConfirmPassword(e.target.value)}
                                />
                                {error && (<ErrorMessage>Passwords must be the same</ErrorMessage>)}
                            </InputDiv>
                        </TabPanel>

                        {/* Additional Information */}
                        <TabPanel>
                            {/* Change first name */}
                            <InputDiv>
                                <Labels htmlFor="input">Change Your First Name</Labels>
                                <input
                                    type="text"
                                    placeholder="Change Your First Name"
                                    value={changefirstname}
                                    onChange={e => saveChangeFirstName(e.target.value)}
                                />
                            </InputDiv>

                            {/* Change last name */}
                            <InputDiv>
                                <Labels htmlFor="input">Change Your Last Name</Labels>
                                <input
                                    type="text"
                                    placeholder="Change Your Last Name"
                                    value={changelastname}
                                    onChange={e => saveChangeLastName(e.target.value)}
                                />
                            </InputDiv>

                            {/* Change your address */}
                            <InputDiv>
                                <Labels htmlFor="input">Modify Your Address</Labels>
                                <input
                                    type="text"
                                    placeholder="Modify Your Current Address"
                                    value={modifyaddress}
                                    onChange={e => saveModifyAddress(e.target.value)}
                                />
                            </InputDiv>
                            {/* Change your country */}
                            <InputDiv>
                                <Labels htmlFor="input">Change Your Country</Labels>
                                <SelectInput
                                    type="text"
                                    value={country}
                                    onChange={e => selectCountry(e.target.value)}
                                >
                                    <option
                                        value="Germany"
                                    >Germany</option>
                                    <option
                                        value="Switzerland">Switzerland</option>
                                    <option
                                        value="Austria">Austria</option>
                                </SelectInput>
                            </InputDiv>
                        </TabPanel>
                    </Tabs>
                    <ButtonDiv>Update</ButtonDiv>
                </DivTabs>
            </form>
        </>
    )
}

export default TabsTable;
