import React from 'react'
import styled from '@emotion/styled';
import { css } from '@emotion/core';

const Links = styled.p`
    color: var(--black);
`;

const MainContainerAside = styled.aside`
    /* margin: 6rem 7rem; */
    line-height: 2rem;
    a{
        text-decoration: none;
        color: var(--grey2);
    }
`;


const Aside = () => {
    return (
        <div>
            <MainContainerAside>
                <Links><i className="tiny material-icons">home</i>{'    '}<a href="!#">Home</a></Links>
                <Links><i className="tiny material-icons">settings_voice</i>{'    '}<a href="!#">My Account</a></Links>
                <Links><i className="tiny material-icons">business</i>{'    '}<a href="!#">My Company</a></Links>
                <Links><i className="tiny material-icons">settings</i>{'    '}<a href="!#">My Settings</a></Links>
                <Links><i className="tiny material-icons">event_note</i>{'    '}<a href="!#">News</a></Links>
                <Links><i className="tiny material-icons">insert_chart</i>{'    '}<a href="!#">Analytics</a></Links>
            </MainContainerAside>
        </div>
    )
}

export default Aside;
