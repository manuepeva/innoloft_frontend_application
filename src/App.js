import React from 'react';
import Aside from './Components/Aside/Aside';
import Form from './Components/Form/Form';
import Heading from './Components/Heading/Heading';
import './Components/Styles/index.css';
import styled from '@emotion/styled';
import { Provider } from 'react-redux';
import store from './Redux/Store';


const ContainerBody = styled.div`
    display: flex;
    justify-content: space-around;
    margin: 5rem 8rem;
    @media(max-width:768px){
      display: contents;
      margin: 2rem;
      padding: 2rem;
      max-width: 100%;
    }
    @media(max-width:960px){
      display: block;
    }
`;


function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Heading />
        <ContainerBody>
          <Aside />
          <Form />
        </ContainerBody>
      </div>
    </Provider>
  );
}

export default App;
