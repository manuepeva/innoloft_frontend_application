import {
    SHOW_ALERT,
    HIDE_ALERT,
    UPDATE_ADD_INFO,
    UPDATE_CHANGE_EMAIL,
    UPDATE_CHANGE_PASSWORD,
    UPDATE_CONFIRM_PASSWORD,
    CHANGE_FIRST_NAME,
    CHANGE_LAST_NAME,
    MODIFY_ADDRESS,
    CHANGE_YOUR_COUNTRY
} from '../Types/index';

const initialState = {
    email: '',
    password: [],
    confirmpassword: [],
    changefirstname: [],
    changelastname: [],
    modifyaddress: [],
    country: [],
    error: false

}

export default function (state = initialState, action) {
    switch (action.type) {
        case UPDATE_CHANGE_EMAIL:
            return {
                ...state,

            }
        case SHOW_ALERT:
            return {
                ...state,
                error: false
            }
        default:
            return state;
    }
}