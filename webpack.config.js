const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode: 'development',
    devtool: 'none',
    entry: './src/index.js',
    output: {
        filename: 'Innoloft.js',
        path: path.resolve(__dirname, 'Innoloft')
    },

    module: {
        rules: [
            {
                test: /\.jsx|js?$/,
                exclude: '/node_modules/',
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env',
                            '@babel/preset-react'
                        ],
                    },
                },
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif|pdf)$/i,
                use: {
                    loader: 'file-loader',
                    options: {
                        limit: 8192,
                    }
                },
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ],
            },
            {
                test: /\.html$/i,
                use: {
                    loader: 'html-loader'
                },
            },
        ],
    },
    devServer: {
        contentBase: path.join(__dirname, 'Gardenias'),
        hot: true,
        open: true
    },
    plugins: [new HtmlWebpackPlugin({
        title: 'Portfolio',
        template: './public/index.html'
    })]
};